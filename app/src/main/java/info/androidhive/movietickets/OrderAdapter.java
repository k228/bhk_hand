package info.androidhive.movietickets;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.zip.Inflater;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    ArrayList<info.androidhive.movietickets.Product> products; ArrayList<info.androidhive.movietickets.Stock> stocks;
    OnClickListener onClickListener;
    public OrderAdapter(ArrayList<info.androidhive.movietickets.Product> products, ArrayList<info.androidhive.movietickets.Stock> stocks,OnClickListener onClickListener) {
        this.stocks=stocks;
        this.products=products;
        this.onClickListener=onClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collect_product_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.e("OrderAdapter", "onBindViewHolder: "+position );
        holder.bind(products.get(position),stocks);
    }


    @Override
    public int getItemCount() {
        Log.e("OrderAdapter", "getItemCount: "+products.size() );
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView productImage;
        TextView hashId,boxNumber,oDate,productName,barcode;
        Button save;
        RecyclerView stockRecycleView;
        GridLayoutManager gridLayoutManager;
        StockAdapter stockAdapter;
        ArrayList<info.androidhive.movietickets.Stock> stocksList=new ArrayList<>();
        public ViewHolder(View itemView) {
            super(itemView);
            productImage=itemView.findViewById(R.id.productImage);
            hashId=itemView.findViewById(R.id.hashId);
            boxNumber=itemView.findViewById(R.id.boxNumber);
            oDate=itemView.findViewById(R.id.oDate);
            productName=itemView.findViewById(R.id.productName);
            barcode=itemView.findViewById(R.id.barcode);
            stockRecycleView=itemView.findViewById(R.id.stockRecycelView);
            save=itemView.findViewById(R.id.save);
        }

        public void bind(final info.androidhive.movietickets.Product product, final ArrayList<info.androidhive.movietickets.Stock> stocks) {
            Picasso.with(itemView.getContext())
                    .load("https://www.bahook.com"+"/image/pic/new/"+ product.getImage()+"/1/md.jpg")
                    .placeholder(R.drawable.bahook)
                    .into(productImage);

            hashId.setText(product.getHashID());
            if(product.getBoxNumber()!=null){
                boxNumber.setText(product.getBoxNumber().toString());
            }
            oDate.setText(getJalaliDateTime(product.getODate()));
            if(product.getName()!=null){
                productName.setText( product.getName()+ Html.fromHtml( "<font color=#d2d6de> ( "+product.getCount()+" )</font>"));
            }else{
                productName.setText(product.getLName()+ Html.fromHtml( "<font color=#d2d6de> ( "+product.getCount()+" )</font>"));
            }
            barcode.setText(product.getBarcode().toString());



            gridLayoutManager=new GridLayoutManager(itemView.getContext(),2);
            stockAdapter=new StockAdapter(stocksList);

            stockRecycleView.setAdapter(stockAdapter);
            stockRecycleView.setLayoutManager(gridLayoutManager);

            for(info.androidhive.movietickets.Stock stock:stocks){
                try {
                    Method method = product.getClass().getMethod("getOrderProductsStock"+stock.getAbbr(), null);
                    Integer max = (Integer) method.invoke(product, null);
                    if(max>0){
                        stock.setMax(max);
                        Method method2 = product.getClass().getMethod("getStock"+stock.getAbbr()+"Collected", null);
                        Integer total = (Integer) method2.invoke(product, null);
                        Log.e("OrderAdapter", "bind: "+total );
                        stock.setTotal(total);
                        stocksList.add(stock);
                    }
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }



            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.save(stocksList,product);
                }
            });
        }

        private String getJalaliDateTime(String date) {

            String[] parts = date.split(" ");
            String datePart = parts[0];
            String timePart = parts[1];

            int year;
            int month;
            int day;

            String[] dateParts = datePart.split("-");
            year = Integer.parseInt( dateParts[0]);
            month = Integer.parseInt( dateParts[1]);
            day = Integer.parseInt( dateParts[2]);

            CalendarTool calendarTool=new CalendarTool();
            calendarTool.setGregorianDate(year,month,day);

            return calendarTool.getIranianDate() +" "+timePart;

        }
    }

    public interface OnClickListener{

        void save(ArrayList<info.androidhive.movietickets.Stock> stocksList, info.androidhive.movietickets.Product product);
    }
}
