package info.androidhive.movietickets;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TicketResultActivity extends AppCompatActivity {
    private static final String TAG = TicketResultActivity.class.getSimpleName();

    // url to search barcode
    private static final String URL = "https://www.bahook.com";
//    private static final String URL = "http://94.183.117.225/";

    private TextView  txtError;
    private ProgressBar progressBar;

    LinearLayoutManager linearLayoutManager;
    RecyclerView ordersRecycleView;
    OrderAdapter orderAdapter;
    ArrayList<info.androidhive.movietickets.Product> products=new ArrayList<>();
    ArrayList<info.androidhive.movietickets.Stock> stocks=new ArrayList<>();
    private String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_result);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtError = findViewById(R.id.txt_error);
        progressBar = findViewById(R.id.progressBar);
        ordersRecycleView = findViewById(R.id.ordersRecycleView);

        String barcode = getIntent().getStringExtra("code");

        // close the activity in case of empty barcode
        if (TextUtils.isEmpty(barcode)) {
            Toast.makeText(getApplicationContext(), "Barcode is empty!", Toast.LENGTH_LONG).show();
            finish();
        }

        linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        orderAdapter=new OrderAdapter(products, stocks, new OrderAdapter.OnClickListener() {
            @Override
            public void save(ArrayList<info.androidhive.movietickets.Stock> stocksList, info.androidhive.movietickets.Product product) {
                sendCollectedProducts(stocksList,product);
            }
        });
        ordersRecycleView.setLayoutManager(linearLayoutManager);
        ordersRecycleView.setAdapter(orderAdapter);

        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");

        // search the barcode
        searchBarcode(barcode);
    }

    private void sendCollectedProducts(final ArrayList<info.androidhive.movietickets.Stock> stocksList, final info.androidhive.movietickets.Product product) {
        StringRequest jsonObjRequest=new StringRequest(Request.Method.POST, URL+"/app/v1/hand/orders/product/collect", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getBaseContext(),"انجام شد",Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getBaseContext(),"خطا در بر قراری ارتباط",Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", token);
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> postParam = new HashMap<>();
                postParam.put("orderId", product.getOrderId().toString());
                postParam.put("productId", product.getProductId().toString());

                Integer total = 0;
                for (info.androidhive.movietickets.Stock stock:stocksList){
                    postParam.put(stock.getAbbr(), stock.getTotal().toString());
                    total+=stock.getTotal();
                }

                postParam.put("total", total.toString());



                return postParam;
            }
        };
        MyApplication.getInstance().addToRequestQueue(jsonObjRequest);
    }

    /**
     * Searches the barcode by making http call
     * Request was made using Volley network library but the library is
     * not suggested in production, consider using Retrofit
     */
    private void searchBarcode(final String barcode) {

        StringRequest jsonObjRequest=new StringRequest(Request.Method.GET, URL+"/app/v1/hand/orders/product/"+barcode, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: "+response );
                renderMovie(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: ",error );
                Toast.makeText(getBaseContext(),"خطا در بر قراری ارتباط",Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", token);
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> postParam = new HashMap<>();

                postParam.put("barcode", barcode);
                return postParam;
            }
        };
        MyApplication.getInstance().addToRequestQueue(jsonObjRequest);

    }

    private void showNoTicket() {
        txtError.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        ordersRecycleView.setVisibility(View.GONE);
    }

    /**
     * Rendering movie details on the ticket
     * @param response
     */
    private void renderMovie(String response) {
        try {
            // converting json to movie object
            info.androidhive.movietickets.Order order = new Gson().fromJson(response, info.androidhive.movietickets.Order.class);
            Log.e(TAG, "renderMovie: "+order.getProducts().size() );
            if (order != null) {
                products.addAll(order.getProducts());
                stocks.addAll(order.getStocks());
                orderAdapter.notifyDataSetChanged();


                ordersRecycleView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            } else {
                // movie not found
                showNoTicket();
            }
        } catch (JsonSyntaxException e) {
            Log.e(TAG, "JSON Exception: " + e.getMessage());
            showNoTicket();
            Toast.makeText(getApplicationContext(), "Error occurred. Check your LogCat for full report", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // exception
            showNoTicket();
            Toast.makeText(getApplicationContext(), "Error occurred. Check your LogCat for full report", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
