
package info.androidhive.movietickets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stock {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("abbr")
    @Expose
    private String abbr;
    @SerializedName("isForeignStock")
    @Expose
    private Integer isForeignStock;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("max")
    @Expose
    private Integer max;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("phone_for_sms")
    @Expose
    private Object phoneForSms;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public Integer getIsForeignStock() {
        return isForeignStock;
    }

    public void setIsForeignStock(Integer isForeignStock) {
        this.isForeignStock = isForeignStock;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Object getPhoneForSms() {
        return phoneForSms;
    }

    public void setPhoneForSms(Object phoneForSms) {
        this.phoneForSms = phoneForSms;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
