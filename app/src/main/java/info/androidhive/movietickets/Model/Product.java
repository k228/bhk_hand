
package info.androidhive.movietickets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("barcode")
    @Expose
    private Long barcode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lName")
    @Expose
    private String lName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("boxNumber")
    @Expose
    private Integer boxNumber;
    @SerializedName("oDate")
    @Expose
    private String oDate;
    @SerializedName("hashID")
    @Expose
    private String hashID;
    @SerializedName("ATR")
    @Expose
    private String aTR;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("price_id")
    @Expose
    private Integer priceId;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("approved_count")
    @Expose
    private Integer approvedCount;
    @SerializedName("order_products_stock_20")
    @Expose
    private Integer orderProductsStock20;
    @SerializedName("order_products_stock_21")
    @Expose
    private Integer orderProductsStock21;
    @SerializedName("order_products_stock_25")
    @Expose
    private Integer orderProductsStock25;
    @SerializedName("order_products_stock_26")
    @Expose
    private Integer orderProductsStock26;
    @SerializedName("order_products_stock_30")
    @Expose
    private Integer orderProductsStock30;
    @SerializedName("order_products_stock_22")
    @Expose
    private Integer orderProductsStock22;
    @SerializedName("order_products_stock_23")
    @Expose
    private Integer orderProductsStock23;
    @SerializedName("order_products_stock_96")
    @Expose
    private Integer orderProductsStock96;
    @SerializedName("order_products_stock_97")
    @Expose
    private Integer orderProductsStock97;
    @SerializedName("order_products_stock_98")
    @Expose
    private Integer orderProductsStock98;
    @SerializedName("order_products_stock_99")
    @Expose
    private Integer orderProductsStock99;
    @SerializedName("collected")
    @Expose
    private Integer collected;
    @SerializedName("stock_20_collected")
    @Expose
    private Integer stock20Collected;
    @SerializedName("stock_21_collected")
    @Expose
    private Integer stock21Collected;
    @SerializedName("stock_25_collected")
    @Expose
    private Integer stock25Collected;
    @SerializedName("stock_26_collected")
    @Expose
    private Integer stock26Collected;
    @SerializedName("stock_30_collected")
    @Expose
    private Integer stock30Collected;
    @SerializedName("stock_22_collected")
    @Expose
    private Integer stock22Collected;
    @SerializedName("stock_23_collected")
    @Expose
    private Integer stock23Collected;
    @SerializedName("stock_96_collected")
    @Expose
    private Integer stock96Collected;
    @SerializedName("stock_97_collected")
    @Expose
    private Integer stock97Collected;
    @SerializedName("stock_98_collected")
    @Expose
    private Integer stock98Collected;
    @SerializedName("stock_99_collected")
    @Expose
    private Integer stock99Collected;
    @SerializedName("Aprice")
    @Expose
    private Integer aprice;
    @SerializedName("Adiscount")
    @Expose
    private Integer adiscount;
    @SerializedName("disc_id")
    @Expose
    private Integer discId;
    @SerializedName("Aweight")
    @Expose
    private Integer aweight;
    @SerializedName("cDate")
    @Expose
    private String cDate;
    @SerializedName("uDate")
    @Expose
    private String uDate;
    @SerializedName("post_Id")
    @Expose
    private Object postId;
    @SerializedName("gift_id")
    @Expose
    private Integer giftId;
    @SerializedName("detail")
    @Expose
    private Object detail;
    @SerializedName("no_products_collected")
    @Expose
    private Integer noProductsCollected;

    public Long getBarcode() {
        return barcode;
    }

    public void setBarcode(Long barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLName() {
        return lName;
    }

    public void setLName(String lName) {
        this.lName = lName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getBoxNumber() {
        return boxNumber;
    }

    public void setBoxNumber(Integer boxNumber) {
        this.boxNumber = boxNumber;
    }

    public String getODate() {
        return oDate;
    }

    public void setODate(String oDate) {
        this.oDate = oDate;
    }

    public String getHashID() {
        return hashID;
    }

    public void setHashID(String hashID) {
        this.hashID = hashID;
    }

    public String getATR() {
        return aTR;
    }

    public void setATR(String aTR) {
        this.aTR = aTR;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getPriceId() {
        return priceId;
    }

    public void setPriceId(Integer priceId) {
        this.priceId = priceId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getApprovedCount() {
        return approvedCount;
    }

    public void setApprovedCount(Integer approvedCount) {
        this.approvedCount = approvedCount;
    }

    public Integer getOrderProductsStock20() {
        return orderProductsStock20;
    }

    public void setOrderProductsStock20(Integer orderProductsStock20) {
        this.orderProductsStock20 = orderProductsStock20;
    }

    public Integer getOrderProductsStock21() {
        return orderProductsStock21;
    }

    public void setOrderProductsStock21(Integer orderProductsStock21) {
        this.orderProductsStock21 = orderProductsStock21;
    }

    public Integer getOrderProductsStock25() {
        return orderProductsStock25;
    }

    public void setOrderProductsStock25(Integer orderProductsStock25) {
        this.orderProductsStock25 = orderProductsStock25;
    }

    public Integer getOrderProductsStock26() {
        return orderProductsStock26;
    }

    public void setOrderProductsStock26(Integer orderProductsStock26) {
        this.orderProductsStock26 = orderProductsStock26;
    }

    public Integer getOrderProductsStock30() {
        return orderProductsStock30;
    }

    public void setOrderProductsStock30(Integer orderProductsStock30) {
        this.orderProductsStock30 = orderProductsStock30;
    }

    public Integer getOrderProductsStock22() {
        return orderProductsStock22;
    }

    public void setOrderProductsStock22(Integer orderProductsStock22) {
        this.orderProductsStock22 = orderProductsStock22;
    }

    public Integer getOrderProductsStock23() {
        return orderProductsStock23;
    }

    public void setOrderProductsStock23(Integer orderProductsStock23) {
        this.orderProductsStock23 = orderProductsStock23;
    }

    public Integer getOrderProductsStock96() {
        return orderProductsStock96;
    }

    public void setOrderProductsStock96(Integer orderProductsStock96) {
        this.orderProductsStock96 = orderProductsStock96;
    }

    public Integer getOrderProductsStock97() {
        return orderProductsStock97;
    }

    public void setOrderProductsStock97(Integer orderProductsStock97) {
        this.orderProductsStock97 = orderProductsStock97;
    }

    public Integer getOrderProductsStock98() {
        return orderProductsStock98;
    }

    public void setOrderProductsStock98(Integer orderProductsStock98) {
        this.orderProductsStock98 = orderProductsStock98;
    }

    public Integer getOrderProductsStock99() {
        return orderProductsStock99;
    }

    public void setOrderProductsStock99(Integer orderProductsStock99) {
        this.orderProductsStock99 = orderProductsStock99;
    }

    public Integer getCollected() {
        return collected;
    }

    public void setCollected(Integer collected) {
        this.collected = collected;
    }

    public Integer getStock20Collected() {
        return stock20Collected;
    }

    public void setStock20Collected(Integer stock20Collected) {
        this.stock20Collected = stock20Collected;
    }

    public Integer getStock21Collected() {
        return stock21Collected;
    }

    public void setStock21Collected(Integer stock21Collected) {
        this.stock21Collected = stock21Collected;
    }

    public Integer getStock25Collected() {
        return stock25Collected;
    }

    public void setStock25Collected(Integer stock25Collected) {
        this.stock25Collected = stock25Collected;
    }

    public Integer getStock26Collected() {
        return stock26Collected;
    }

    public void setStock26Collected(Integer stock26Collected) {
        this.stock26Collected = stock26Collected;
    }

    public Integer getStock30Collected() {
        return stock30Collected;
    }

    public void setStock30Collected(Integer stock30Collected) {
        this.stock30Collected = stock30Collected;
    }

    public Integer getStock22Collected() {
        return stock22Collected;
    }

    public void setStock22Collected(Integer stock22Collected) {
        this.stock22Collected = stock22Collected;
    }

    public Integer getStock23Collected() {
        return stock23Collected;
    }

    public void setStock23Collected(Integer stock23Collected) {
        this.stock23Collected = stock23Collected;
    }

    public Integer getStock96Collected() {
        return stock96Collected;
    }

    public void setStock96Collected(Integer stock96Collected) {
        this.stock96Collected = stock96Collected;
    }

    public Integer getStock97Collected() {
        return stock97Collected;
    }

    public void setStock97Collected(Integer stock97Collected) {
        this.stock97Collected = stock97Collected;
    }

    public Integer getStock98Collected() {
        return stock98Collected;
    }

    public void setStock98Collected(Integer stock98Collected) {
        this.stock98Collected = stock98Collected;
    }

    public Integer getStock99Collected() {
        return stock99Collected;
    }

    public void setStock99Collected(Integer stock99Collected) {
        this.stock99Collected = stock99Collected;
    }

    public Integer getAprice() {
        return aprice;
    }

    public void setAprice(Integer aprice) {
        this.aprice = aprice;
    }

    public Integer getAdiscount() {
        return adiscount;
    }

    public void setAdiscount(Integer adiscount) {
        this.adiscount = adiscount;
    }

    public Integer getDiscId() {
        return discId;
    }

    public void setDiscId(Integer discId) {
        this.discId = discId;
    }

    public Integer getAweight() {
        return aweight;
    }

    public void setAweight(Integer aweight) {
        this.aweight = aweight;
    }

    public String getCDate() {
        return cDate;
    }

    public void setCDate(String cDate) {
        this.cDate = cDate;
    }

    public String getUDate() {
        return uDate;
    }

    public void setUDate(String uDate) {
        this.uDate = uDate;
    }

    public Object getPostId() {
        return postId;
    }

    public void setPostId(Object postId) {
        this.postId = postId;
    }

    public Integer getGiftId() {
        return giftId;
    }

    public void setGiftId(Integer giftId) {
        this.giftId = giftId;
    }

    public Object getDetail() {
        return detail;
    }

    public void setDetail(Object detail) {
        this.detail = detail;
    }

    public Integer getNoProductsCollected() {
        return noProductsCollected;
    }

    public void setNoProductsCollected(Integer noProductsCollected) {
        this.noProductsCollected = noProductsCollected;
    }

}
