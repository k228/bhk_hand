package info.androidhive.movietickets;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class StockAdapter extends RecyclerView.Adapter<StockAdapter.ViewHolder> {
    ArrayList<info.androidhive.movietickets.Stock> stocks;
    public StockAdapter(ArrayList<info.androidhive.movietickets.Stock> stocks) {
        this.stocks=stocks;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("StockAdapter", "onCreateViewHolder: " );
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            holder.bind(stocks.get(position));
        } catch (Exception e) {
            Log.e("StockAdapter", "onBindViewHolder: ",e );
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return stocks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Spinner stockSpinner;
        TextView stockName;
        public ViewHolder(View itemView) {
            super(itemView);
            stockSpinner=itemView.findViewById(R.id.stockSpinner);
            stockName=itemView.findViewById(R.id.stockName);
        }

        public void bind(final info.androidhive.movietickets.Stock stock) throws Exception {
            Log.e("StockAdapter", "bind: "  );
            stockName.setText(stock.getName());
            // Spinner click listener
            stockSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    stock.setTotal(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            List<String> counts = new ArrayList<String>();
            for(int i = 0; i<= stock.getMax(); i++){
                counts.add(String.valueOf(i));
            }
            Log.e("StockAdapter", "bind: "+stock.getTotal() );




            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(itemView.getContext(), android.R.layout.simple_spinner_item, counts);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            stockSpinner.setAdapter(dataAdapter);
            stockSpinner.setSelection(stock.getTotal());
        }
    }

    public static Object getAttribute(Object obj, String name) throws Exception {
        Field field = obj.getClass().getDeclaredField(name);
        field.setAccessible(true);
        return field.get(obj);
    }
}
