package info.androidhive.movietickets;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String URL = "https://www.bahook.com";
    private LinearLayout loginContainer;
    private Button buttonScan;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                    this, Manifest.permission.CAMERA)) {


            } else {
                ActivityCompat.requestPermissions((Activity) this,
                        new String[]{Manifest.permission.CAMERA},
                        200);
            }

        }



        // making toolbar transparent
        transparentToolbar();

        setContentView(R.layout.activity_main);

        loginContainer = (LinearLayout)findViewById(R.id.login_container);
        buttonScan = (Button)findViewById(R.id.btn_scan);
        progressBar = (ProgressBar)findViewById(R.id.progressBar2);

        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", "");
        if(!token.equals("")){
            progressBar.setVisibility(View.VISIBLE);
            checkToken(token);
        }else{
            progressBar.setVisibility(View.GONE);
            loginContainer.setVisibility(View.VISIBLE);
            buttonScan.setVisibility(View.GONE);
        }

        findViewById(R.id.btn_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ScanActivity.class));
            }
        });
    }

    private void checkToken(final String token){
        StringRequest jsonObjRequest=new StringRequest(Request.Method.GET, URL+"/app/v1/hand/auth", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("checktoken", "onResponse: "+response );
                SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("token", response);
                editor.apply();

                progressBar.setVisibility(View.GONE);
                loginContainer.setVisibility(View.GONE);
                buttonScan.setVisibility(View.VISIBLE);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getBaseContext(),"خطا در بر قراری ارتباط",Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                loginContainer.setVisibility(View.VISIBLE);
                buttonScan.setVisibility(View.GONE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token);
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }
        };
        MyApplication.getInstance().addToRequestQueue(jsonObjRequest);
    }

    public void login(View view) {


        StringRequest jsonObjRequest=new StringRequest(Request.Method.POST, URL+"/app/v1/hand/auth", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("token", response);
                editor.apply();
                LinearLayout loginContainer = (LinearLayout)findViewById(R.id.login_container);
                progressBar.setVisibility(View.GONE);
                loginContainer.setVisibility(View.GONE);
                buttonScan.setVisibility(View.VISIBLE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getBaseContext(),"خطا در بر قراری ارتباط",Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                loginContainer.setVisibility(View.VISIBLE);
                buttonScan.setVisibility(View.GONE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json; charset=UTF-8");
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                EditText username = (EditText)findViewById(R.id.editText1);
                EditText password = (EditText)findViewById(R.id.editText2);
                Log.e("", "getParams: "+username.getText() );

                Map<String, String> postParam = new HashMap<>();

                postParam.put("email", username.getText().toString());
                postParam.put("password", password.getText().toString());
                return postParam;
            }
        };
        MyApplication.getInstance().addToRequestQueue(jsonObjRequest);

    }

    private void transparentToolbar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }
}
